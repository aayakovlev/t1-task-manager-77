# TASK MANAGER

Simple application for project and task management.

## DEVELOPER

**NAME**: Yakovlev Anton

**E-MAIL**: aayakovlev@t1-consulting.ru

## SOFTWARE

**OS**: Windows 10 LTSC 1809 (build 17763.2628)

**JDK**: OpenJDK 1.8.0_41

**MAVEN**: 3.6.3

## HARDWARE

**CPU**: AMD R9

**RAM**: 32 Gb

**SSD**: NVMe 512 Gb

## APPLICATION PROPERTIES

Same as standard **Spring Boot** properties from modules like JPA, Web, etc.

For k8s uses db properties placed in OS environments.

| OS ENV      | EXAMPLE VALUE                                 | MEANING                  |
|-------------|-----------------------------------------------|--------------------------|
| DB_URL      | jdbc:postgresql://127.0.0.1:5432/task_manager | database connection url  |
| DB_USERNAME | postgres                                      | database user            |
| DB_PASSWORD | postgres                                      | database password        |

## BUILD APPLICATION

````sh
mvn clean install
````

## RUN APPLICATION

### FROM IDE

````sh
mvn spring-boot:run
````

### STANDALONE

````sh
java -jar task-manager.war
````
