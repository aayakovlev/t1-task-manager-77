package ru.t1.aayakovlev.tm.integration.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import ru.t1.aayakovlev.tm.dto.model.ResultDTO;
import ru.t1.aayakovlev.tm.dto.model.TaskDTO;
import ru.t1.aayakovlev.tm.dto.model.UserDTO;
import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.marker.IntegrationCategory;

import java.net.HttpCookie;
import java.util.Arrays;
import java.util.List;

import static ru.t1.aayakovlev.tm.constant.dto.TaskDTOTestConstant.*;

@Category(IntegrationCategory.class)
public class TaskRestEndpointTest {

    @NotNull
    private static final String BASE_URL = "http://localhost:8080/api/tasks/";

    @NotNull
    private static final HttpHeaders header = new HttpHeaders();

    @Nullable
    private static String sessionId;

    @Nullable
    private static String userId;

    @BeforeClass
    public static void beforeClass() throws JsonProcessingException {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = "http://localhost:8080/api/auth/login?username=test&password=test";
        header.setContentType(MediaType.APPLICATION_JSON);
        @NotNull final ResponseEntity<ResultDTO> response = restTemplate.postForEntity(url, new HttpEntity<>(""), ResultDTO.class);
        Assert.assertEquals(200, response.getStatusCodeValue());
        Assert.assertNotNull(response.getBody());
        Assert.assertTrue(response.getBody().getSuccess());
        @NotNull final HttpHeaders headersResponse = response.getHeaders();
        List<HttpCookie> cookies = HttpCookie.parse(headersResponse.getFirst(HttpHeaders.SET_COOKIE));
        sessionId = cookies.stream()
                .filter(item -> "JSESSIONID".equals(item.getName())
                ).findFirst().get().getValue();
        Assert.assertNotNull(sessionId);
        header.put(HttpHeaders.COOKIE, Arrays.asList("JSESSIONID=" + sessionId));
        @NotNull final String urlProfile = "http://localhost:8080/api/auth/profile";
        @NotNull final ResponseEntity<UserDTO> responseProfile = restTemplate.exchange(urlProfile, HttpMethod.GET, new HttpEntity<>(header), UserDTO.class);
        userId = responseProfile.getBody().getId();
    }

    private static ResponseEntity<List> sendRequestList(@NotNull final String url, @NotNull final HttpMethod method, @NotNull final HttpEntity<List> httpEntity) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, httpEntity, List.class);
    }

    private static ResponseEntity<TaskDTO> sendRequest(@NotNull final String url, @NotNull final HttpMethod method, @NotNull final HttpEntity<TaskDTO> httpEntity) {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        return restTemplate.exchange(url, method, httpEntity, TaskDTO.class);
    }

    @AfterClass
    public static void afterClass() {
        @NotNull final RestTemplate restTemplate = new RestTemplate();
        @NotNull final String url = "http://localhost:8080/api/auth/logout";
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(header));
    }

    @Before
    public void before() {
        @NotNull final String url = BASE_URL + "add/";
        TASK_ONE.setUserId(userId);
        TASK_TWO.setUserId(userId);
        TASK_THREE.setUserId(userId);
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(TASK_ONE, header));
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(TASK_TWO, header));
    }

    @After
    public void after() {
        @NotNull final String url = BASE_URL + "deleteAll/";
        sendRequestList(url, HttpMethod.POST, new HttpEntity<>(header));
    }

    @Test
    public void findAllTest() throws Exception {
        @NotNull final String url = BASE_URL + "findAll/";
        Assert.assertEquals(2, sendRequestList(url, HttpMethod.GET, new HttpEntity<>(header)).getBody().size());
    }

    @Test
    public void addTest() throws Exception {
        @NotNull final String url = BASE_URL + "add/";
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(TASK_THREE, header));
        @NotNull final String findUrl = BASE_URL + "findById/" + TASK_THREE.getId();
        ;
        Assert.assertNotNull(sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(header)).getBody());
    }

    @Test
    public void saveTest() throws Exception {
        @NotNull final String url = BASE_URL + "save/";
        TASK_ONE.setStatus(Status.IN_PROGRESS);
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(TASK_ONE, header));
        @NotNull final String findUrl = BASE_URL + "findById/" + TASK_ONE.getId();
        Assert.assertEquals(Status.IN_PROGRESS, sendRequest(findUrl, HttpMethod.GET, new HttpEntity<>(header)).getBody().getStatus());
    }

    @Test
    public void findByIdTest() throws Exception {
        @NotNull final String url = BASE_URL + "findById/" + TASK_ONE.getId();
        Assert.assertNotNull(sendRequest(url, HttpMethod.GET, new HttpEntity<>(header)).getBody());
    }

    @Test
    public void deleteTest() throws Exception {
        @NotNull final String url = BASE_URL + "delete/";
        sendRequest(url, HttpMethod.POST, new HttpEntity<>(TASK_ONE, header));
        @NotNull final String findUrl = BASE_URL + "findAll/";
        Assert.assertEquals(1, sendRequestList(findUrl, HttpMethod.GET, new HttpEntity<>(header)).getBody().size());
    }

    @Test
    public void deleteAllTest() throws Exception {
        @NotNull final String url = BASE_URL + "deleteAll/";
        sendRequestList(url, HttpMethod.POST, new HttpEntity<>(header));
        @NotNull final String findUrl = BASE_URL + "findAll/";
        Assert.assertEquals(0, sendRequestList(findUrl, HttpMethod.GET, new HttpEntity<>(header)).getBody().size());
    }

}
