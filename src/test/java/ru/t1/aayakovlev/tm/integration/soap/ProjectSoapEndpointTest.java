package ru.t1.aayakovlev.tm.integration.soap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.data.util.CastUtils;
import org.springframework.http.HttpHeaders;
import ru.t1.aayakovlev.tm.api.endpoint.soap.IAuthSoapEndpoint;
import ru.t1.aayakovlev.tm.api.endpoint.soap.IProjectSoapEndpoint;
import ru.t1.aayakovlev.tm.client.soap.AuthSoapEndpointClient;
import ru.t1.aayakovlev.tm.client.soap.ProjectSoapEndpointClient;
import ru.t1.aayakovlev.tm.dto.model.ProjectDTO;
import ru.t1.aayakovlev.tm.dto.soap.*;
import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.marker.IntegrationCategory;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import java.net.MalformedURLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static ru.t1.aayakovlev.tm.constant.dto.ProjectDTOTestConstant.PROJECT_ONE;
import static ru.t1.aayakovlev.tm.constant.dto.ProjectDTOTestConstant.PROJECT_TWO;
import static ru.t1.aayakovlev.tm.constant.dto.ProjectDTOTestConstant.PROJECT_THREE;

@Category(IntegrationCategory.class)
public class ProjectSoapEndpointTest {

    @NotNull
    private static final String URL = "http://localhost:8080";

    @NotNull
    private static IAuthSoapEndpoint authEndpoint;

    @NotNull
    private static IProjectSoapEndpoint projectEndpoint;

    @Nullable
    private static String USER_ID;

    @BeforeClass
    public static void beforeClass() throws MalformedURLException {
        authEndpoint = AuthSoapEndpointClient.getInstance(URL);
        Assert.assertTrue(authEndpoint.login(new AuthLoginRequest("test", "test")).isResult());
        projectEndpoint = ProjectSoapEndpointClient.getInstance(URL);
        @NotNull final BindingProvider authBindingProvider = (BindingProvider) authEndpoint;
        @NotNull final BindingProvider projectBindingProvider = (BindingProvider) projectEndpoint;
        @NotNull Map<String, List<String>> headers = CastUtils.cast((Map) authBindingProvider.getResponseContext().get(MessageContext.HTTP_RESPONSE_HEADERS));
        if (headers == null) headers = new HashMap<String, List<String>>();
        @NotNull final Object cookieValue = headers.get(HttpHeaders.SET_COOKIE);
        @NotNull final List<String> cookies = (List<String>) cookieValue;
        headers.put("Cookie", Collections.singletonList(cookies.get(0)));
        projectBindingProvider.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS, headers);
        USER_ID = authEndpoint.profile(new AuthProfileRequest()).getUser().getId();
    }

    @Before
    public void before() throws Exception {
        PROJECT_ONE.setUserId(USER_ID);
        PROJECT_TWO.setUserId(USER_ID);
        PROJECT_THREE.setUserId(USER_ID);
        projectEndpoint.save(new ProjectSaveRequest(PROJECT_ONE));
        projectEndpoint.save(new ProjectSaveRequest(PROJECT_TWO));
    }

    @Test
    public void findAllTest() throws Exception {
        @Nullable final List<ProjectDTO> projects = projectEndpoint.findAll(new ProjectFindAllRequest()).getProject();
        Assert.assertEquals(2, projects.size());
    }

    @After
    public void after() throws Exception {
        projectEndpoint.deleteAll(new ProjectDeleteAllRequest());
    }

    @Test
    public void addTest() throws Exception {
        projectEndpoint.save(new ProjectSaveRequest(PROJECT_THREE));
        Assert.assertNotNull(projectEndpoint.findById(new ProjectFindByIdRequest(PROJECT_THREE.getId())));
    }

    @Test
    public void saveTest() throws Exception {
        PROJECT_ONE.setStatus(Status.IN_PROGRESS);
        projectEndpoint.save(new ProjectSaveRequest(PROJECT_ONE));
        @Nullable final ProjectDTO project = projectEndpoint.findById(new ProjectFindByIdRequest(PROJECT_ONE.getId())).getProject();
        Assert.assertNotNull(project);
        Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());
    }

    @Test
    public void findByIdTest() throws Exception {
        Assert.assertNotNull(projectEndpoint.findById(new ProjectFindByIdRequest(PROJECT_ONE.getId())));
    }

    @Test
    public void deleteTest() throws Exception {
        projectEndpoint.deleteById(new ProjectDeleteByIdRequest(PROJECT_ONE.getId()));
        Assert.assertNotNull(projectEndpoint.findById(new ProjectFindByIdRequest(PROJECT_ONE.getId())));
    }

    @Test
    public void deleteAllTest() throws Exception {
        projectEndpoint.deleteAll(new ProjectDeleteAllRequest());
        Assert.assertNotNull(projectEndpoint.findAll(new ProjectFindAllRequest()));
    }

}
