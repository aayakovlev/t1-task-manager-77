package ru.t1.aayakovlev.tm.unit.service.dto;

import lombok.SneakyThrows;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.t1.aayakovlev.tm.api.service.dto.IProjectDTOService;
import ru.t1.aayakovlev.tm.marker.UnitCategory;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import ru.t1.aayakovlev.tm.api.service.dto.ITaskDTOService;
import ru.t1.aayakovlev.tm.dto.model.TaskDTO;
import ru.t1.aayakovlev.tm.exception.entity.TaskNotFoundException;
import ru.t1.aayakovlev.tm.util.UserUtil;

import java.util.List;

import static ru.t1.aayakovlev.tm.constant.dto.ProjectDTOTestConstant.PROJECT_ONE;
import static ru.t1.aayakovlev.tm.constant.dto.TaskDTOTestConstant.*;
import static ru.t1.aayakovlev.tm.constant.dto.UserDTOTestConstant.USER_LOGIN;
import static ru.t1.aayakovlev.tm.constant.dto.UserDTOTestConstant.USER_PASSWORD;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@Category(UnitCategory.class)
public class TaskDTOServiceTest {

    @NotNull
    @Autowired
    private ITaskDTOService service;

    @NotNull
    @Autowired
    private IProjectDTOService projectDTOService;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @Before
    @SneakyThrows
    public void init() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(USER_LOGIN, USER_PASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        TASK_ONE.setUserId(UserUtil.getUserId());
        TASK_ONE.setProjectId(PROJECT_ONE.getId());
        TASK_TWO.setUserId(UserUtil.getUserId());
        TASK_TWO.setProjectId(PROJECT_ONE.getId());
        PROJECT_ONE.setUserId(UserUtil.getUserId());
        projectDTOService.save(PROJECT_ONE);
        service.save(TASK_ONE);
        service.save(TASK_TWO);
    }

    @After
    @SneakyThrows
    public void finish() {
        service.deleteAllByUserId(UserUtil.getUserId());
        projectDTOService.deleteAllByUserId(UserUtil.getUserId());
    }

    @Test
    @SneakyThrows
    public void count() {
        Assert.assertEquals(2, service.countByUserId(UserUtil.getUserId()));
    }

    @Test
    @SneakyThrows
    public void deleteAll() {
        service.deleteAllByUserId(UserUtil.getUserId());
        Assert.assertEquals(0, service.countByUserId(UserUtil.getUserId()));
    }

    @Test
    public void deleteById() {
        Assert.assertThrows( TaskNotFoundException.class, () ->
                service.findByUserIdAndId(UserUtil.getUserId(), TASK_THREE.getId())
        );
    }

    @Test
    @SneakyThrows
    public void existsById() {
        Assert.assertTrue(service.existsByUserIdAndId(UserUtil.getUserId(), TASK_ONE.getId()));
    }

    @Test
    @SneakyThrows
    public void findAll() {
        @NotNull final List<TaskDTO> tasks = service.findAllByUserId(UserUtil.getUserId());
        Assert.assertEquals(2, tasks.size());
    }

    @Test
    @SneakyThrows
    public void findAllByProjectId() {
        @NotNull final List<TaskDTO> tasks = service.findAllByUserIdAndProjectId(UserUtil.getUserId(), PROJECT_ONE.getId());
        Assert.assertEquals(2, tasks.size());
    }

    @Test
    @SneakyThrows
    public void findById() {
        @NotNull final TaskDTO task = service
                .findByUserIdAndId(UserUtil.getUserId(), TASK_ONE.getId());
        Assert.assertEquals(TASK_ONE.getId(), task.getId());
        Assert.assertEquals(TASK_ONE.getName(), task.getName());
        Assert.assertEquals(TASK_ONE.getDescription(), task.getDescription());
        Assert.assertEquals(TASK_ONE.getStatus(), task.getStatus());
        Assert.assertEquals(TASK_ONE.getCreated(), task.getCreated());
        Assert.assertEquals(TASK_ONE.getUserId(), task.getUserId());
    }

}
