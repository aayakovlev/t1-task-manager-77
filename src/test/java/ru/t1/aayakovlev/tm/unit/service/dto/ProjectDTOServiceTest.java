package ru.t1.aayakovlev.tm.unit.service.dto;

import lombok.SneakyThrows;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.t1.aayakovlev.tm.marker.UnitCategory;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import ru.t1.aayakovlev.tm.api.service.dto.IProjectDTOService;
import ru.t1.aayakovlev.tm.dto.model.ProjectDTO;
import ru.t1.aayakovlev.tm.exception.entity.ProjectNotFoundException;
import ru.t1.aayakovlev.tm.util.UserUtil;

import java.util.List;

import static ru.t1.aayakovlev.tm.constant.dto.ProjectDTOTestConstant.*;
import static ru.t1.aayakovlev.tm.constant.dto.UserDTOTestConstant.USER_LOGIN;
import static ru.t1.aayakovlev.tm.constant.dto.UserDTOTestConstant.USER_PASSWORD;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@Category(UnitCategory.class)
public class ProjectDTOServiceTest {

    @NotNull
    @Autowired
    private IProjectDTOService service;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @Before
    @SneakyThrows
    public void init() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(USER_LOGIN, USER_PASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        PROJECT_ONE.setUserId(UserUtil.getUserId());
        PROJECT_TWO.setUserId(UserUtil.getUserId());
        PROJECT_THREE.setUserId(UserUtil.getUserId());
        service.save(PROJECT_ONE);
        service.save(PROJECT_TWO);
    }

    @After
    @SneakyThrows
    public void finish() {
        service.deleteAllByUserId(UserUtil.getUserId());
    }

    @Test
    public void count() {
        Assert.assertEquals(2, service.countByUserId(UserUtil.getUserId()));
    }

    @Test
    @SneakyThrows
    public void deleteAll() {
        service.deleteAllByUserId(UserUtil.getUserId());
        Assert.assertEquals(0, service.countByUserId(UserUtil.getUserId()));
    }

    @Test
    @SneakyThrows
    public void deleteById() {
        Assert.assertThrows(ProjectNotFoundException.class, () ->
                service.findByUserIdAndId(UserUtil.getUserId(), PROJECT_THREE.getId())
        );
    }

    @Test
    @SneakyThrows
    public void existsById() {
        Assert.assertTrue(service.existsByUserIdAndId(UserUtil.getUserId(), PROJECT_ONE.getId()));
    }

    @Test
    @SneakyThrows
    public void findAll() {
        @NotNull final List<ProjectDTO> projects = service.findAllByUserId(UserUtil.getUserId());
        Assert.assertEquals(2, projects.size());
    }

    @Test
    @SneakyThrows
    public void findById() {
        @NotNull final ProjectDTO project = service
                .findByUserIdAndId(UserUtil.getUserId(), PROJECT_ONE.getId());
        Assert.assertEquals(PROJECT_ONE.getId(), project.getId());
        Assert.assertEquals(PROJECT_ONE.getName(), project.getName());
        Assert.assertEquals(PROJECT_ONE.getDescription(), project.getDescription());
        Assert.assertEquals(PROJECT_ONE.getStatus(), project.getStatus());
        Assert.assertEquals(PROJECT_ONE.getCreated(), project.getCreated());
        Assert.assertEquals(PROJECT_ONE.getUserId(), project.getUserId());
    }

}
