package ru.t1.aayakovlev.tm.unit.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.t1.aayakovlev.tm.dto.model.UserDTO;
import ru.t1.aayakovlev.tm.marker.UnitCategory;

import javax.annotation.Resource;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ru.t1.aayakovlev.tm.constant.dto.UserDTOTestConstant.USER_LOGIN;
import static ru.t1.aayakovlev.tm.constant.dto.UserDTOTestConstant.USER_PASSWORD;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@Category(UnitCategory.class)
public class AuthRestEndpointTest {

    @NotNull
    @Resource
    private AuthenticationManager authenticationManager;

    @NotNull
    @Autowired
    private WebApplicationContext wac;

    @NotNull
    private MockMvc mockMvc;

    @NotNull
    private static final String AUTH_URL = "http://localhost:8080/api/auth";

    @Before
    public void init() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(USER_LOGIN, USER_PASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    @After
    @SneakyThrows
    public void finish() {
    }

    @Test
    @SneakyThrows
    public void logout() {
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(AUTH_URL + "/logout")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        Assert.assertTrue(objectMapper.readValue(json, Boolean.class));
    }

    @Test
    @SneakyThrows
    public void profile() {
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(AUTH_URL + "/profile")
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final UserDTO user = objectMapper.readValue(json, UserDTO.class);
        Assert.assertNotNull(user);
    }

}
