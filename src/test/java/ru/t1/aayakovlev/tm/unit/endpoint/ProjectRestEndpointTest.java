package ru.t1.aayakovlev.tm.unit.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.t1.aayakovlev.tm.marker.UnitCategory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.util.NestedServletException;
import ru.t1.aayakovlev.tm.api.service.dto.IProjectDTOService;
import ru.t1.aayakovlev.tm.dto.model.ProjectDTO;
import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.util.UserUtil;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

import static ru.t1.aayakovlev.tm.constant.dto.ProjectDTOTestConstant.*;
import static ru.t1.aayakovlev.tm.constant.dto.UserDTOTestConstant.USER_LOGIN;
import static ru.t1.aayakovlev.tm.constant.dto.UserDTOTestConstant.USER_PASSWORD;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@Category(UnitCategory.class)
public class ProjectRestEndpointTest {

    @NotNull
    @Resource
    private AuthenticationManager authenticationManager;

    @NotNull
    @Autowired
    private WebApplicationContext wac;

    @NotNull
    private MockMvc mockMvc;

    @NotNull
    @Autowired
    private IProjectDTOService service;

    @NotNull
    private static final String PROJECT_URL = "http://localhost:8080/api/projects";

    @Before
    public void init() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(USER_LOGIN, USER_PASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        PROJECT_ONE.setUserId(UserUtil.getUserId());
        add(PROJECT_ONE);
        add(PROJECT_TWO);
    }

    @After
    @SneakyThrows
    public void finish() {
        service.deleteAllByUserId(UserUtil.getUserId());
    }

    @SneakyThrows
    private void add(@NotNull final ProjectDTO project) {
        @NotNull final String url = PROJECT_URL + "/save";
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(project);
        System.out.println(json);
        mockMvc.perform(MockMvcRequestBuilders.put(url)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Nullable
    @SneakyThrows
    private ProjectDTO findById(@NotNull final String id) {
        @NotNull final String url = PROJECT_URL + "/find/" + id;
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        if ("".equals(json)) return null;
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, ProjectDTO.class);
    }

    @Test
    @SneakyThrows
    public void count() {
        @NotNull final String url = PROJECT_URL + "/count";
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        Assert.assertEquals(2, objectMapper.readValue(json, Long.class).longValue());
    }

    @Test
    @SneakyThrows
    public void deleteById() {
        @NotNull final String url = PROJECT_URL + "/delete/" + PROJECT_ONE.getId();
        mockMvc.perform(MockMvcRequestBuilders.delete(url)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
        Assert.assertThrows(
                NestedServletException.class,
                () -> findById(PROJECT_FOUR.getId())
        );
    }

    @Test
    @SneakyThrows
    public void existsById(){
        @NotNull final String url = PROJECT_URL + "/exists/" + PROJECT_ONE.getId();
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        Assert.assertTrue(objectMapper.readValue(json, Boolean.class));
    }

    @Test
    @SneakyThrows
    public void findAll() {
        @NotNull final String url = PROJECT_URL + "/find";
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @Nullable final List<ProjectDTO> projects = Arrays.asList(objectMapper.readValue(json, ProjectDTO[].class));
        Assert.assertNotNull(projects);
        Assert.assertEquals(2, projects.size());
    }

    @Test
    @SneakyThrows
    public void findById() {
        @NotNull final ProjectDTO project = findById(PROJECT_ONE.getId());
        Assert.assertEquals(PROJECT_ONE.getId(), project.getId());
        Assert.assertEquals(PROJECT_ONE.getName(), project.getName());
        Assert.assertEquals(PROJECT_ONE.getDescription(), project.getDescription());
        Assert.assertEquals(PROJECT_ONE.getUserId(), project.getUserId());
        Assert.assertEquals(PROJECT_ONE.getStatus(), project.getStatus());
        Assert.assertEquals(PROJECT_ONE.getCreated(), project.getCreated());
    }

    @Test
    @SneakyThrows
    public void save(){
        add(PROJECT_THREE);
        @Nullable final ProjectDTO project = findById(PROJECT_THREE.getId());
        Assert.assertNotNull(project);
    }

    @Test
    @SneakyThrows
    public void update() {
        PROJECT_TWO.setStatus(Status.IN_PROGRESS);
        @NotNull final String url = PROJECT_URL + "/save/"+PROJECT_TWO.getId();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(PROJECT_TWO);
        mockMvc.perform(MockMvcRequestBuilders.post(url)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andDo(print())
                .andExpect(status().isOk());
        @Nullable final ProjectDTO project = findById(PROJECT_TWO.getId());
        Assert.assertEquals(project.getStatus(), Status.IN_PROGRESS);
    }

}
