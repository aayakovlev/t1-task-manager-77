package ru.t1.aayakovlev.tm.unit.repository.model;

import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.t1.aayakovlev.tm.marker.UnitCategory;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.aayakovlev.tm.model.Project;
import ru.t1.aayakovlev.tm.model.User;
import ru.t1.aayakovlev.tm.repository.model.ProjectRepository;
import ru.t1.aayakovlev.tm.repository.model.UserRepository;
import ru.t1.aayakovlev.tm.util.UserUtil;

import java.util.List;

import static ru.t1.aayakovlev.tm.constant.dto.UserDTOTestConstant.USER_LOGIN;
import static ru.t1.aayakovlev.tm.constant.dto.UserDTOTestConstant.USER_PASSWORD;
import static ru.t1.aayakovlev.tm.constant.model.ProjectTestConstant.*;

@Transactional
@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@Category(UnitCategory.class)
public class ProjectRepositoryTest {

    @NotNull
    @Autowired
    private ProjectRepository repository;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    @Autowired
    private UserRepository userRepository;

    @NotNull
    private User user;

    @Before
    public void init() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(USER_LOGIN, USER_PASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        user = userRepository.findById(UserUtil.getUserId()).get();
        PROJECT_ONE.setUser(user);
        PROJECT_TWO.setUser(user);
        PROJECT_THREE.setUser(user);
        repository.save(PROJECT_ONE);
        repository.save(PROJECT_TWO);
    }

    @After
    public void finish() {
        repository.deleteAll();
    }

    @Test
    public void count() {
        Assert.assertEquals(2, repository.countByUser(userRepository.findById(UserUtil.getUserId()).get()));
    }

    @Test
    @Transactional
    public void deleteAll() {
        repository.deleteAllByUser(user);
        Assert.assertEquals(0, repository.countByUser(user));
    }

    @Test
    @Transactional
    public void deleteById() {
        repository.deleteByUserAndId(user, PROJECT_ONE.getId());
        Assert.assertNull(
                repository.findByUserAndId(user, PROJECT_ONE.getId()).orElse(null)
        );
    }

    @Test
    public void existsById() {
        Assert.assertTrue(repository.existsByUserAndId(user, PROJECT_ONE.getId()));
    }

    @Test
    public void findAll() {
        @NotNull final List<Project> projects = repository.findAllByUser(user);
        Assert.assertEquals(2, projects.size());
    }

    @Test
    public void findById() {
        @NotNull final Project project = repository
                .findByUserAndId(user, PROJECT_ONE.getId())
                .get();
        Assert.assertEquals(PROJECT_ONE.getId(), project.getId());
        Assert.assertEquals(PROJECT_ONE.getName(), project.getName());
        Assert.assertEquals(PROJECT_ONE.getDescription(), project.getDescription());
        Assert.assertEquals(PROJECT_ONE.getStatus(), project.getStatus());
        Assert.assertEquals(PROJECT_ONE.getCreated(), project.getCreated());
    }

}
