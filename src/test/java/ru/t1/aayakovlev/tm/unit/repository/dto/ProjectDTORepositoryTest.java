package ru.t1.aayakovlev.tm.unit.repository.dto;

import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.t1.aayakovlev.tm.marker.UnitCategory;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.aayakovlev.tm.dto.model.ProjectDTO;
import ru.t1.aayakovlev.tm.repository.dto.ProjectDTORepository;
import ru.t1.aayakovlev.tm.util.UserUtil;

import java.util.List;

import static ru.t1.aayakovlev.tm.constant.dto.ProjectDTOTestConstant.*;
import static ru.t1.aayakovlev.tm.constant.dto.UserDTOTestConstant.USER_LOGIN;
import static ru.t1.aayakovlev.tm.constant.dto.UserDTOTestConstant.USER_PASSWORD;

@Transactional
@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@Category(UnitCategory.class)
public class ProjectDTORepositoryTest {

    @NotNull
    @Autowired
    private ProjectDTORepository repository;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @Before
    public void init() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(USER_LOGIN, USER_PASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        PROJECT_ONE.setUserId(UserUtil.getUserId());
        PROJECT_TWO.setUserId(UserUtil.getUserId());
        PROJECT_THREE.setUserId(UserUtil.getUserId());
        repository.save(PROJECT_ONE);
        repository.save(PROJECT_TWO);
    }

    @After
    public void finish() {
        repository.deleteAll();
    }

    @Test
    public void count() {
        Assert.assertEquals(2, repository.countByUserId(UserUtil.getUserId()));
    }

    @Test
    @Transactional
    public void deleteAll() {
        repository.deleteAllByUserId(UserUtil.getUserId());
        Assert.assertEquals(0, repository.countByUserId(UserUtil.getUserId()));
    }

    @Test
    public void deleteById() {
        Assert.assertNull(
                repository.findByUserIdAndId(UserUtil.getUserId(), PROJECT_THREE.getId()).orElse(null)
        );
    }

    @Test
    public void existsById() {
        Assert.assertTrue(repository.existsByUserIdAndId(UserUtil.getUserId(), PROJECT_ONE.getId()));
    }

    @Test
    public void findAll() {
        @NotNull final List<ProjectDTO> projects = repository.findAllByUserId(UserUtil.getUserId());
        Assert.assertEquals(2, projects.size());
    }

    @Test
    public void findById() {
        @NotNull final ProjectDTO project = repository
                .findByUserIdAndId(UserUtil.getUserId(), PROJECT_ONE.getId())
                .get();
        Assert.assertEquals(PROJECT_ONE.getId(), project.getId());
        Assert.assertEquals(PROJECT_ONE.getName(), project.getName());
        Assert.assertEquals(PROJECT_ONE.getDescription(), project.getDescription());
        Assert.assertEquals(PROJECT_ONE.getStatus(), project.getStatus());
        Assert.assertEquals(PROJECT_ONE.getCreated(), project.getCreated());
        Assert.assertEquals(PROJECT_ONE.getUserId(), project.getUserId());
    }

}
