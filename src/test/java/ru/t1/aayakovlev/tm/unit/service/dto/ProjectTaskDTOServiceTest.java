package ru.t1.aayakovlev.tm.unit.service.dto;

import lombok.SneakyThrows;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.t1.aayakovlev.tm.marker.UnitCategory;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import ru.t1.aayakovlev.tm.api.service.dto.IProjectDTOService;
import ru.t1.aayakovlev.tm.api.service.dto.IProjectTaskDTOService;
import ru.t1.aayakovlev.tm.api.service.dto.ITaskDTOService;
import ru.t1.aayakovlev.tm.util.UserUtil;

import static ru.t1.aayakovlev.tm.constant.dto.ProjectDTOTestConstant.PROJECT_ONE;
import static ru.t1.aayakovlev.tm.constant.dto.ProjectDTOTestConstant.PROJECT_TWO;
import static ru.t1.aayakovlev.tm.constant.dto.TaskDTOTestConstant.TASK_ONE;
import static ru.t1.aayakovlev.tm.constant.dto.TaskDTOTestConstant.TASK_TWO;
import static ru.t1.aayakovlev.tm.constant.dto.UserDTOTestConstant.USER_LOGIN;
import static ru.t1.aayakovlev.tm.constant.dto.UserDTOTestConstant.USER_PASSWORD;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@Category(UnitCategory.class)
public class ProjectTaskDTOServiceTest {

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    @Autowired
    private IProjectTaskDTOService service;

    @NotNull
    @Autowired
    private IProjectDTOService projectService;

    @NotNull
    @Autowired
    private ITaskDTOService taskService;

    @Before
    @SneakyThrows
    public void init() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(USER_LOGIN, USER_PASSWORD);
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        PROJECT_ONE.setUserId(UserUtil.getUserId());
        PROJECT_TWO.setUserId(UserUtil.getUserId());
        projectService.save(PROJECT_ONE);
        projectService.save(PROJECT_TWO);
        TASK_ONE.setUserId(UserUtil.getUserId());
        TASK_ONE.setProjectId(PROJECT_ONE.getId());
        TASK_TWO.setUserId(UserUtil.getUserId());
        TASK_TWO.setProjectId(PROJECT_TWO.getId());
        taskService.save(TASK_ONE);
        taskService.save(TASK_TWO);
    }

    @After
    @SneakyThrows
    public void finish() {
        taskService.deleteAllByUserId(UserUtil.getUserId());
        projectService.deleteAllByUserId(UserUtil.getUserId());
    }

    @Test
    @SneakyThrows
    public void deleteByProjectId() {
        service.deleteByUserIdAndId(UserUtil.getUserId(), PROJECT_ONE.getId());
        Assert.assertEquals(0, taskService.findAllByUserIdAndProjectId(UserUtil.getUserId(), PROJECT_ONE.getId()).size());
    }

    @Test
    @SneakyThrows
    public void deleteAllProjects() {
        service.deleteAllByUserId(UserUtil.getUserId());
        Assert.assertEquals(0, taskService.findAllByUserIdAndProjectId(UserUtil.getUserId(), PROJECT_ONE.getId()).size());
        Assert.assertEquals(0, taskService.findAllByUserIdAndProjectId(UserUtil.getUserId(), PROJECT_TWO.getId()).size());
    }

}
