package ru.t1.aayakovlev.tm.constant.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.dto.model.ProjectDTO;

public final class ProjectDTOTestConstant {

    @NotNull
    public static final ProjectDTO PROJECT_ONE = new ProjectDTO("First");

    @NotNull
    public static final ProjectDTO PROJECT_TWO = new ProjectDTO("Second");

    @NotNull
    public static final ProjectDTO PROJECT_THREE = new ProjectDTO("Third");

    @NotNull
    public static final ProjectDTO PROJECT_FOUR = new ProjectDTO("Fourth");

}
