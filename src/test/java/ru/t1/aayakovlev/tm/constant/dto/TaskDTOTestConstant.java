package ru.t1.aayakovlev.tm.constant.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.dto.model.TaskDTO;

public final class TaskDTOTestConstant {

    @NotNull
    public static final TaskDTO TASK_ONE = new TaskDTO("First");

    @NotNull
    public static final TaskDTO TASK_TWO = new TaskDTO("Second");

    @NotNull
    public static final TaskDTO TASK_THREE = new TaskDTO("Third");

    @NotNull
    public static final TaskDTO TASK_FOUR = new TaskDTO("Fourth");

}
