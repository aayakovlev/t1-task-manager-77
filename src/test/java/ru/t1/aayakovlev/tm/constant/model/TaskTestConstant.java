package ru.t1.aayakovlev.tm.constant.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.model.Task;

public final class TaskTestConstant {

    @NotNull
    public static final Task TASK_ONE = new Task("First");

    @NotNull
    public static final Task TASK_TWO = new Task("Second");

    @NotNull
    public static final Task TASK_THREE = new Task("Third");

    @NotNull
    public static final Task TASK_FOUR = new Task("Fourth");

}
