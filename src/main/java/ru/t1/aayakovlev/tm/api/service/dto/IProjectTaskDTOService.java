package ru.t1.aayakovlev.tm.api.service.dto;

import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.aayakovlev.tm.exception.AbstractException;

public interface IProjectTaskDTOService {

    @Transactional
    void deleteByUserIdAndId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws AbstractException;

    @Transactional
    void deleteAllByUserId(
            @Nullable final String userId
    ) throws AbstractException;

}
