package ru.t1.aayakovlev.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityEmptyException;
import ru.t1.aayakovlev.tm.model.Project;

import java.util.List;

public interface IProjectService {

    long countByUserId(@Nullable final String userId) throws AbstractException;

    @Transactional
    void deleteAllByUserId(@Nullable final String userId) throws AbstractException;

    @Transactional
    void deleteByUserIdAndId(@Nullable final String userId, @Nullable final String id) throws AbstractException;

    boolean existsByUserIdAndId(@Nullable final String userId, @Nullable final String id) throws AbstractException;

    @NotNull
    List<Project> findAllByUserId(@Nullable final String userId) throws AbstractException;

    @NotNull
    Project findByUserIdAndId(@Nullable final String userId, @Nullable final String id) throws AbstractException;

    @NotNull
    @Transactional
    Project save(@Nullable final Project project) throws EntityEmptyException;

}
