package ru.t1.aayakovlev.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityEmptyException;
import ru.t1.aayakovlev.tm.model.User;

import java.util.List;

public interface IUserService {

    long count() throws AbstractException;

    @Transactional
    void deleteAll();

    @Transactional
    void deleteById(@Nullable final String id) throws AbstractException;

    boolean existsById(@Nullable final String id) throws AbstractException;

    @NotNull
    List<User> findAll() throws AbstractException;

    @NotNull
    User findById(@Nullable final String id) throws AbstractException;

    @Nullable
    User findByLogin(@NotNull final String login);

    @NotNull
    @Transactional
    User save(@Nullable final User user) throws EntityEmptyException;

}
