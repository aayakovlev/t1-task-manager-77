package ru.t1.aayakovlev.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class ResultDTO {

    @NotNull
    private Boolean success = true;

    @Nullable
    private String message = "";

    public ResultDTO(@NotNull final Boolean success) {
        this.success = success;
    }

    public ResultDTO(@NotNull final Exception e) {
        success = false;
        message = e.getMessage();
    }


}
