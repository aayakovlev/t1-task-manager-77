package ru.t1.aayakovlev.tm.dto.soap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.dto.model.ProjectDTO;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@XmlType(name = "")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "projectFindAllResponse")
public class ProjectFindAllResponse {

    protected List<ProjectDTO> project;

    public ProjectFindAllResponse(@NotNull final List<ProjectDTO> project) {
        this.project = project;
    }

}
