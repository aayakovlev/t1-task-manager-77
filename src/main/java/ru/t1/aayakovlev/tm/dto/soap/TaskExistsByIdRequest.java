package ru.t1.aayakovlev.tm.dto.soap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.annotation.*;

@Getter
@Setter
@NoArgsConstructor
@XmlType(name = "")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "taskExistsByIdRequest")
public class TaskExistsByIdRequest {

    @XmlElement(required = true)
    protected String id;

    public TaskExistsByIdRequest(@NotNull final String id) {
        this.id = id;
    }

}
