package ru.t1.aayakovlev.tm.dto.soap;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.annotation.*;

@Getter
@Setter
@NoArgsConstructor
@XmlType(name = "")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "taskFindAllByProjectIdRequest")
public class TaskFindAllByProjectIdRequest {

    @XmlElement(required = true)
    protected String projectId;

    public TaskFindAllByProjectIdRequest(@NotNull final String projectId) {
        this.projectId = projectId;
    }

}
