package ru.t1.aayakovlev.tm.dto.model;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.persistence.*;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "tm_user", schema = "public", catalog = "task_manager")
public final class UserDTO {

    private static final long serialVersionUID = 1;

    @Id
    @NotNull
    @Column(name = "id", columnDefinition = "VARCHAR(36)")
    private String id = UUID.randomUUID().toString();

    @NotNull
    @Column(name = "login", columnDefinition = "VARCHAR(64)")
    private String login;

    @NotNull
    @Column(name = "password", columnDefinition = "VARCHAR(255)")
    private String passwordHash;

}
