package ru.t1.aayakovlev.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.aayakovlev.tm.api.service.dto.IProjectDTOService;
import ru.t1.aayakovlev.tm.api.service.dto.IProjectTaskDTOService;
import ru.t1.aayakovlev.tm.dto.model.CustomUser;
import ru.t1.aayakovlev.tm.dto.model.ProjectDTO;
import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityEmptyException;

@Controller
@RequestMapping("/projects")
public class ProjectController {

    @Autowired
    private IProjectDTOService service;

    @Autowired
    private IProjectTaskDTOService projectTaskService;

    @GetMapping("")
    @Secured({"ROLE_USER", "ROLE_ADMINISTRATOR"})
    public ModelAndView index(
            @AuthenticationPrincipal @NotNull final CustomUser user
    ) throws AbstractException {
        return new ModelAndView("project-list", "projects", service.findAllByUserId(user.getUserId()));
    }

    @GetMapping("/create")
    @Secured({"ROLE_USER", "ROLE_ADMINISTRATOR"})
    public String create(
            @AuthenticationPrincipal @NotNull final CustomUser user
    ) throws EntityEmptyException {
        @NotNull final ProjectDTO project = new ProjectDTO("new Project " + System.currentTimeMillis());
        project.setUserId(user.getUserId());
        service.save(project);
        return "redirect:/projects";
    }

    @GetMapping("/delete/{id}")
    @Secured({"ROLE_USER", "ROLE_ADMINISTRATOR"})
    public String delete(
            @AuthenticationPrincipal @NotNull final CustomUser user,
            @PathVariable("id") @Nullable final String id
    ) throws AbstractException {
        projectTaskService.deleteByUserIdAndId(user.getUserId(), id);
        return "redirect:/projects";
    }

    @PostMapping("/edit/{id}")
    @Secured({"ROLE_USER", "ROLE_ADMINISTRATOR"})
    public String edit(
            @AuthenticationPrincipal @NotNull final CustomUser user,
            @ModelAttribute("project") @NotNull final ProjectDTO project,
            @NotNull final BindingResult result
    ) throws EntityEmptyException {
        project.setUserId(user.getUserId());
        service.save(project);
        return "redirect:/projects";
    }

    @GetMapping("/edit/{id}")
    @Secured({"ROLE_USER", "ROLE_ADMINISTRATOR"})
    public ModelAndView edit(
            @AuthenticationPrincipal @NotNull final CustomUser user,
            @PathVariable("id") @Nullable final String id
    ) throws AbstractException {
        @NotNull final ProjectDTO project = service.findByUserIdAndId(user.getUserId(), id);
        @NotNull final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("project-edit");
        modelAndView.addObject("project", project);
        modelAndView.addObject("statuses", Status.values());
        return modelAndView;
    }

}
