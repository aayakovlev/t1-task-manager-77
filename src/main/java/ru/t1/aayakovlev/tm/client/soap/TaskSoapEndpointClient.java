package ru.t1.aayakovlev.tm.client.soap;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.api.endpoint.soap.ITaskSoapEndpoint;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;

public final class TaskSoapEndpointClient {

    public static ITaskSoapEndpoint getInstance(@NotNull final String baseURL)
            throws MalformedURLException {
        @NotNull final String wsdl = baseURL + "/ws/TaskEndpoint?wsdl";
        @NotNull final URL url = new URL(wsdl);
        @NotNull final String lp = "TaskRestEndpointImplService";
        @NotNull final String ns = "http://endpoint.tm.vmironova.t1consulting.ru/";
        @NotNull final QName name = new QName(ns, lp);
        @NotNull final ITaskSoapEndpoint result = Service.create(url, name).getPort(ITaskSoapEndpoint.class);
        @NotNull final BindingProvider bindingProvider = (BindingProvider) result;
        bindingProvider.getRequestContext().put(BindingProvider.SESSION_MAINTAIN_PROPERTY, true);
        return result;
    }

}
