package ru.t1.aayakovlev.tm.client.soap;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.api.endpoint.soap.IAuthSoapEndpoint;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;

public final class AuthSoapEndpointClient {

    public static IAuthSoapEndpoint getInstance(@NotNull final String baseURL)
            throws MalformedURLException {
        @NotNull final String wsdl = baseURL + "/ws/AuthEndpoint?wsdl";
        @NotNull final URL url = new URL(wsdl);
        @NotNull final String lp = "AuthRestEndpointImplService";
        @NotNull final String ns = "http://endpoint.tm.aayakovlev.t1.ru/";
        @NotNull final QName name = new QName(ns, lp);
        @NotNull final IAuthSoapEndpoint result = Service.create(url, name).getPort(IAuthSoapEndpoint.class);
        @NotNull final BindingProvider bindingProvider = (BindingProvider) result;
        bindingProvider.getRequestContext().put(BindingProvider.SESSION_MAINTAIN_PROPERTY, true);
        return result;
    }

}
