package ru.t1.aayakovlev.tm.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.enumerated.RoleType;

import javax.persistence.*;
import java.util.UUID;

@Getter
@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Table(name = "tm_role", schema = "public", catalog = "task_manager")
public final class Role {

    private static final long serialVersionUID = 1;

    @Id
    @NotNull
    @Column(name = "id", columnDefinition = "VARCHAR(36)")
    private String id = UUID.randomUUID().toString();

    @NotNull
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "role_type", columnDefinition = "VARCHAR(64)")
    private RoleType roleType = RoleType.USER;

    @Override
    public String toString() {
        return roleType.name();
    }

}
