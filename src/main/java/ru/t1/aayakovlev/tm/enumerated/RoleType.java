package ru.t1.aayakovlev.tm.enumerated;

public enum RoleType {

    ADMINISTRATOR,
    USER;

}
