package ru.t1.aayakovlev.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.aayakovlev.tm.model.Task;
import ru.t1.aayakovlev.tm.model.User;

import java.util.List;
import java.util.Optional;

@Repository
@Scope("prototype")
public interface TaskRepository extends JpaRepository<Task, String> {

    long countByUser(@Nullable final User user);

    void deleteAllByUser(@Nullable final User user);

    void deleteByUserAndId(@Nullable final User user, @Nullable final String id);

    boolean existsByUserAndId(@Nullable final User user, @Nullable final String id);

    @NotNull
    List<Task> findAllByUser(@Nullable final User user);

    @NotNull
    List<Task> findAllByUserAndProjectId(@Nullable final User user, @Nullable final String projectId);

    @NotNull
    Optional<Task> findByUserAndId(@Nullable final User user, @Nullable final String id);

}
